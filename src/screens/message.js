import React from 'react'
import { StyleSheet, View, ScrollView } from 'react-native';
// ---------------------------------------------------
import Search from '../components/search';
import CardMessage from '../components/cardMessage';
import { Messages } from '../data/demoData';

export default function Message() {
    return (
        <View style={styles.container}>
            <Search />

            {/* show card message */}
            <ScrollView 
                showsVerticalScrollIndicator ={false}
                showsHorizontalScrollIndicator={false}
            >
                {
                    Messages.map((m) => (
                        <CardMessage key={m.id} message={m} />
                    ))
                }
            </ScrollView>
            {/* ----------------- */}
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      margin: 10,
      maxWidth: "100%"
    },
});
