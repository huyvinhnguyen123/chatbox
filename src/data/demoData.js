export const Users = [
    {id: "1", name: "Khanh", avatar: require("../images/user.jpg")},
    {id: "2", name: "Duong", avatar: require("../images/user2.jpg")},
    {id: "3", name: "Huy", avatar: require("../images/user3.jpg")},
    {id: "4", name: "Higuchi", avatar: require("../images/user4.jpeg")},
    {id: "5", name: "Sata", avatar: require("../images/user5.jpg")},
    {id: "6", name: "Chat Box", avatar: require("../images/user6.jpg")},
    {id: "7", name: "Team e-jan", avatar: require("../images/user7.png")}
]

export const Messages = [
    {
        id: "1",
        userId: "1",
        date: "Today", 
        text: "",
        image: "", 
        link: {
            text: "This is link share with someone. You can share this link for other. Please subscribe to this link and join with us for more...",
            image: require("../images/imgBR.jpg")
        }
    },

    {
        id: "2",
        userId: "2",
        date: "Yesterday", 
        text: "GitLabにSourceCodeをアップロードしました。確認お願いします。",
        image: "", 
        link: ""
    },

    {
        id: "3",
        userId: "3",
        date: "Yesterday", 
        text: "GitLabにSourceCodeをアップロードしました。確認お願いします。",
        image: "", 
        link: ""
    },

    {
        id: "4",
        userId: "4",
        date: "Long time ago", 
        text: "Hello",
        image: "", 
        link: ""
    },

    {
        id: "5",
        userId: "5", 
        date: "Yesterday",
        text: "皆さん、お疲れ様です。今、ベトナムの会議ですが、こちらはURLで...",
        image: "", 
        link: ""
    },

    {
        id: "6",
        userId: "6",
        date: "Long time ago", 
        text: "Hello",
        image: "", 
        link: ""
    },

    {
        id: "7",
        userId: "7",
        date: "Long time ago", 
        text: "Hello",
        image: "", 
        link: {
            text: "This is link share with someone. You can share this link for other. Please subscribe to this link and join with us for more...",
            image: require("../images/imgBR.jpg")
        }
    },
]