import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'; 

export default function MessageIcon({message}) {

    const NotifyIcon = () => {
        //first user
        if(message.id === "1"){
            return (
                <>
                    <View style={{ width: "9%" }}>
                        <View style={{marginLeft: 10, borderColor:"#fff", borderWidth: 1, backgroundColor:"red", borderRadius: 10, zIndex: 100}}>
                            <Text style={{color: "#fff", fontSize: 8, textAlign: "center"}}>10</Text>
                        </View>
                        <FontAwesome style={{ marginTop: -8 }} name="bell" size={20} color="#33cccc" />
                    </View>
                    <FontAwesome style={{marginTop: 9, marginLeft: "3%"}} name="eye" size={20} color="#33cccc" />
                    <FontAwesome style={{marginTop: 9, marginLeft: "5%"}} name="ellipsis-h" size={20} color="#33cccc" />
                </>
            )
        }else{
            return (
                <>
                    <FontAwesome style={{ marginTop: 8 }} name="bell" size={20} color="#33cccc" />
                    <FontAwesome style={{marginTop: 9, marginLeft: "5%"}} name="eye" size={20} color="#33cccc" />
                    <FontAwesome style={{marginTop: 9, marginLeft: "5%"}} name="ellipsis-h" size={20} color="#33cccc" />
                </>
            )
        }
    }

    return (
        <View style={styles.iconRight}>
            <View style={{flexDirection: "row", marginEnd: "5%"}}>

                {/* if first user */}
                {
                    NotifyIcon()
                }
                {/* ------------- */}

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    iconRight: {
        alignItems: "flex-end",
        marginTop: 5
    }
})