import React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native';
// ---------------------------------------------------
import { Users } from '../data/demoData';

export default function MessageUser({message}) {
    return (
        <View style={styles.info}>
            <Image
                style={styles.imageUser}
                source={Users.filter((u) => u.id === message?.userId)[0].avatar}
            />
            <View style={styles.infoRight}>
                <Text style={{ fontWeight: 'bold', fontSize: 12 }}>
                    {Users.filter((u) => u.id === message?.userId)[0].name}
                </Text>
                <Text style={{ color: 'gray', fontSize: 10 }}>{message.date}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    info: {
        flexDirection: "row",
    },

    infoRight: {
        marginLeft: 10
    },

    imageUser: {
        borderRadius: 50,
        width: 40,
        height: 40,
        padding: 20
    }
})