import React from 'react'
import { StyleSheet, View, TextInput } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

export default function Search() {
    return (
        <View style={styles.searchSection}>
            <FontAwesome name="search" size={18} color="gray" />
            <TextInput 
                style={styles.input}
                placeholder="Search Message . . ."
            />
        </View>
    )
}

const styles = StyleSheet.create({
    searchSection: {
        padding: 10,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 15,
        marginTop: 15,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#f5f5f5",
        borderRadius: 10,
        width: "90%"
    },

    searchIcon: {
        padding: 5
    },

    input: {
        marginLeft: 15,
        padding: 5,
        width: "90%"
    }
});

