import React from 'react'
import { StyleSheet, View } from 'react-native';
// ---------------------------------------------------
import MessageInfo from './messageInfo';
import MessageIcon from './messageIcon';
import MessageUser from './messageUser';

export default function CardMessage({message}) {
    return (
        <View style={styles.wrapper}>
            
            {/* Component MessageUser */}
            <MessageUser message={message} />
            {/* --------- */}

            {/* Component MessageInfo */}
            <MessageInfo message={message} />
            {/* --------- */}
            
            {/* Component MessageIcon */}
            <MessageIcon message={message} />
            {/* --------- */}

        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 10,
        padding: 10,
        backgroundColor: "#fff",
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5,
    }
    
});
