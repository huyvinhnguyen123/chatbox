import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';

export default function MessageInfo({message}) {

    const changeMessage = () => {
        
        //if user send link
        if(message.link){
            return (
                <View style={{ backgroundColor: "#e6e6e6", flexDirection: "row", padding: 5, marginTop: 10, marginBottom: 10 }}>
                    <Image
                        style={styles.imageMessage}
                        source={message.link.image}
                    />
                    <View style={{ margin: 10, textAlign: 'center', maxWidth: "55%" }}>
                        <Text style={{fontSize: 12, color: 'grey'}}> 
                            {message.link.text}
                        </Text>
                    </View>
                </View>
            )

        //if user send text
        }else{
            return (
                <View style={{ margin: 10, textAlign: 'center', maxWidth: "100%" }}>
                    <Text style={{fontSize: 12, color: 'black'}}> 
                        {message.text}
                    </Text>
                </View>
            )
        }
    }

    return (
        <View style={styles.message}>

            {/* if user send link or text */}
            {
               changeMessage()
            }
            {/* --------------------------*/}

        </View>
    )
}

const styles = StyleSheet.create({
    message: {
        marginTop: 5
    },

    imageMessage: {
        marginTop: 10,
        marginLeft: 10,
        marginBottom: 10,
        width: 130,
        height: 90
    },

})
